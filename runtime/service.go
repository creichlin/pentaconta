package runtime

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"sync"
	"time"

	"gitlab.com/creichlin/pentaconta/declaration"
	"gitlab.com/creichlin/pentaconta/logger"
)

type Service struct {
	service *declaration.Service
	name    string
	logs    logger.Logger
	cmd     *exec.Cmd
	monitor *Monitor
	exit    chan int
}

func NewService(name string, service *declaration.Service, logs logger.Logger) (*Service, error) {
	return &Service{
		name:    name,
		service: service,
		logs:    logs,
		monitor: NewMonitor(),
		exit:    make(chan int, 1),
	}, nil
}

func (e *Service) Log(level int, message string) {
	e.logs.Log(logger.NewLog(level, e.name, e.monitor.GetCount(), message))
}

// Start this service, will restart itself endlessly
func (e *Service) Start() {
	for {
		e.startService()
		select {
		case <-e.exit:
			e.Log(logger.PENTACONTA, "Exited service")
			e.exit <- 1 // send something else to the channel so channels length is greater 0
			return
		case <-time.After(time.Millisecond * 1000):
			// continue
		}
	}
}

func (e *Service) IsTerminated() bool {
	return len(e.exit) > 0
}

func (e *Service) Signal(signal string) {
	if e.monitor.IsStarted() {
		sig, err := parseSignal(signal)
		if err != nil {
			e.Log(logger.PENTACONTA, err.Error())
			return
		}

		err = e.cmd.Process.Signal(sig)
		if err != nil {
			panic(err)
		}
	}
}

func (e *Service) Exit() {
	e.Stop()
	e.exit <- 0
}

func (e *Service) Stop() {
	if e.monitor.IsStarted() {
		go func() {
			// record current terminations
			terminations := e.monitor.GetCount()

			// send int signal
			err := e.cmd.Process.Signal(os.Interrupt)
			if err != nil && err.Error() != "os: process already finished" {
				panic(err)
			}

			// check for 3 seconds if terminated
			for i := 0; i < 10; i++ {
				time.Sleep(time.Millisecond * 300)
				if e.monitor.GetCount() > terminations {
					e.Log(logger.PENTACONTA, "Sigint worked")
					return
				}
			}

			// if termination didn't work, kill it
			e.Log(logger.PENTACONTA, "Sigint did not work, send kill")
			e.cmd.Process.Signal(os.Kill)
		}()
	}
}

// startService is always called from the same thread created by Start
func (e *Service) startService() {
	binary, err := exec.LookPath(e.service.Executable)
	if err != nil {
		e.Log(logger.PENTACONTA, fmt.Sprintf("Could not find executable %v", e.service.Executable))
		return
	}

	e.cmd = exec.Command(binary, e.service.Arguments...)
	e.cmd.Env = os.Environ()

	if e.service.WorkingDir != "" {
		e.cmd.Dir = e.service.WorkingDir
	}

	stdout, err := e.cmd.StdoutPipe()
	if err != nil {
		e.Log(logger.PENTACONTA, err.Error())
		return
	}

	stderr, err := e.cmd.StderrPipe()
	if err != nil {
		e.Log(logger.PENTACONTA, err.Error())
		return
	}

	execErr := e.cmd.Start()
	if execErr != nil {
		e.Log(logger.PENTACONTA, execErr.Error())
		return
	}

	e.Log(logger.PENTACONTA, "Started service")

	wait := &sync.WaitGroup{}
	wait.Add(2)
	go e.logFromPipe(stdout, logger.STDOUT, wait)
	go e.logFromPipe(stderr, logger.STDERR, wait)

	e.monitor.Start()
	defer e.monitor.Stop()

	// we block till the stdout AND stderr reader are finished
	wait.Wait()

	// Wait will release resources and return err
	err = e.cmd.Wait()
	msg := "Terminated service"
	if err != nil {
		msg += " with " + err.Error()
	}
	e.Log(logger.PENTACONTA, msg)
}

// logFromPipe will read output from pipe and send lines to logger
func (e *Service) logFromPipe(pipe io.ReadCloser, level int, wait *sync.WaitGroup) {
	defer wait.Done()
	// this will terminate itself when the command exits
	buffStdout := bufio.NewReader(pipe)
	for {
		line, err := buffStdout.ReadString('\n')
		if err == nil || line != "" {
			e.Log(level, line)
		}
		if err != nil {
			return
		}
	}
}

// Monitor keeps state if service is running
// and the number of terminations with
// thread safe access
type Monitor struct {
	count   int
	running bool
	lock    *sync.Mutex
}

// NewMonitor constructs an initialized zero monitor
func NewMonitor() *Monitor {
	return &Monitor{0, false, &sync.Mutex{}}
}

func (m *Monitor) GetCount() int {
	m.lock.Lock()
	defer m.lock.Unlock()
	return m.count
}

func (m *Monitor) IsStarted() bool {
	m.lock.Lock()
	defer m.lock.Unlock()
	return m.running
}

func (m *Monitor) Start() {
	m.lock.Lock()
	defer m.lock.Unlock()
	if m.running {
		panic("Switch is already on")
	}
	m.running = true
}

func (m *Monitor) Stop() {
	m.lock.Lock()
	defer m.lock.Unlock()
	if !m.running {
		panic("Switch is already off")
	}
	m.count++
	m.running = false
}
