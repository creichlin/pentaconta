package runtime

import (
	"github.com/creichlin/gutil/run"
	"gitlab.com/creichlin/pentaconta/declaration"
)

type FSTrigger struct {
	name     string
	trigger  *declaration.FSTrigger
	services *Runtime
	ftr      *run.FileTriggerRunner
}

func NewFSTrigger(name string, trigger *declaration.FSTrigger, services *Runtime) (*FSTrigger, error) {
	listener := &FSTrigger{
		name:     name,
		trigger:  trigger,
		services: services,
	}
	listener.ftr = run.NewFileTriggerRunner(trigger.Path, false, listener.changed)

	return listener, nil
}

func (f *FSTrigger) changed(event, path string) error {
	for _, service := range f.trigger.Services {
		if f.trigger.Signal == "" {
			f.services.Executors[service].Stop()
		}else{
			f.services.Executors[service].Signal(f.trigger.Signal)
		}
	}
	return nil
}

func (f *FSTrigger) Start() error {
	return f.ftr.Start()
}
