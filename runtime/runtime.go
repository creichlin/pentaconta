package runtime

import (
	"gitlab.com/creichlin/pentaconta/logger"
)

type Runtime struct {
	Logs        logger.Logger
	Executors   map[string]*Service
	FSListeners map[string]*FSTrigger
}
