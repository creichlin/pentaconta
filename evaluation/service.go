package evaluation

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/creichlin/pentaconta/declaration"
	"gitlab.com/creichlin/pentaconta/logger"
)

func Configure(stats *declaration.Stats) (logger.Logger, *EvalService) {
	var logs logger.Logger
	var eval *Collector
	if stats == nil {
		logs = logger.NewStdoutLogger()
		return logs, &EvalService{}
	}

	eval = EvaluationCollector()
	logs = logger.NewSplitLogger(
		logger.NewStdoutLogger(),
		eval,
	)

	return logs, &EvalService{
		collector: eval,
		seconds:   stats.Seconds,
		file:      stats.File,
	}
}

type EvalService struct {
	collector *Collector
	seconds   int
	file      string
}

func (e *EvalService) Loop() {
	if e.collector != nil {
		stats := e.collector.Status(e.seconds)
		bin, err := json.MarshalIndent(stats, "", "  ")
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile(e.file, bin, 0644)
		if err != nil {
			panic(err)
		}
	}
}
