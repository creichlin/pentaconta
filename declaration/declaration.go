package declaration

import (
	"fmt"
	"sort"

	"github.com/creichlin/goschema"
	"github.com/creichlin/gutil"
	"github.com/mitchellh/mapstructure"
)

type Service struct {
	Executable string
	WorkingDir string `mapstructure:"working-dir"`
	Arguments  []string
}

type FSTrigger struct {
	Path     string
	Services []string
	Signal   string
}

type Stats struct {
	File    string
	Seconds int
}

type Root struct {
	Stats      *Stats
	Services   map[string]*Service
	FSTriggers map[string]*FSTrigger `mapstructure:"fs-triggers"`
}

func Doc() string {
	return goschema.Doc(buildSchema())
}

func Parse(data interface{}) (*Root, error) {
	schema := buildSchema()
	errors := goschema.ValidateGO(schema, data)
	if errors.Has() {
		return nil, errors
	}

	root := &Root{}
	err := mapstructure.Decode(data, root)
	errors = validate(root)
	if errors.Has() {
		return nil, errors
	}
	return root, err
}

func validate(r *Root) *gutil.ErrorCollector {
	ec := gutil.NewErrorCollector()

	keys := []string{}
	for name, _ := range r.FSTriggers {
		keys = append(keys, name)
	}
	sort.Strings(keys)

	for _, name := range keys {
		for _, serviceName := range r.FSTriggers[name].Services {
			if _, contains := r.Services[serviceName]; !contains {
				ec.Add(fmt.Errorf("fs-trigger %v has unknown service %v as target", name, serviceName))
			}
		}
	}

	return ec
}

func buildSchema() goschema.Type {
	return goschema.NewObjectType("Pentaconta service declaration", func(o goschema.ObjectType) {
		o.Optional("services").Map(func(m goschema.MapType) {
			m.Object("Services start executables and restart them when terminated/crashed", func(o goschema.ObjectType) {
				o.Attribute("executable").String(
					"path or name, if not absolute will use PATH env var to find binary")
				o.Optional("working-dir").String(
					"path, absolute or relative to current working dir")
				o.Optional("arguments").List(func(l goschema.ListType) {
					l.String("arguments")
				})
			})
		})
		o.Optional("fs-triggers").Map(func(m goschema.MapType) {
			m.Object(
				"filesystem watchers trigger termination (and implicit restart) of services", func(o goschema.ObjectType) {
					o.Attribute("path").String(
						"Folder or file to watch. Folder must exists. If it's a file, parent folder must exist.")
					o.Attribute("services").List(func(l goschema.ListType) {
						l.String("name of service to restart")
					})
					o.Optional("signal").String("Name of the signal to send. If omitted sigint and sigkill are used.")
				})
		})
		o.Optional("stats").Object("statistics writer", func(o goschema.ObjectType) {
			o.Attribute("seconds").Int("defines in which interval stats are written").Min(5).Max(60)
			o.Attribute("file").String("where to write the stats to")
		})
	})
}
