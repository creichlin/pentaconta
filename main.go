package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"os/signal"

	"github.com/ghodss/yaml"
	"gitlab.com/creichlin/pentaconta/declaration"
	"gitlab.com/creichlin/pentaconta/evaluation"
	"gitlab.com/creichlin/pentaconta/logger"
	"gitlab.com/creichlin/pentaconta/runtime"
)

func main() {
	configName, help := readConfigParams()
	if help {
		printHelp()
		return
	}
	location, err := probeLocation(configName)

	if err != nil {
		log.Fatal(err)
	}

	data, err := readData(location)
	if err != nil {
		log.Fatal(err)
	}

	runWithDeclaration(data, time.Hour*24*356*100)
}

func runWithDeclaration(data interface{}, timeout time.Duration) {
	dec, err := declaration.Parse(data)
	if err != nil {
		log.Fatal(err)
	}

	logs, eval := evaluation.Configure(dec.Stats)

	services := &runtime.Runtime{
		Logs:        logs,
		Executors:   map[string]*runtime.Service{},
		FSListeners: map[string]*runtime.FSTrigger{},
	}

	createAndStartServices(services, dec)
	createAndStartFsTriggers(services, dec)

	exitChannel := make(chan os.Signal, 1)
	signal.Notify(exitChannel, os.Interrupt)

	// eval loop is called every second
	evalTicker := time.NewTicker(time.Second)
	defer evalTicker.Stop()

	// wait for expiration
	expired := time.After(timeout)

	exit := func() {
		logs.Log(logger.Log{
			Instance: 0,
			Message:  "INT signal, quitting",
			Service:  "pentaconta",
			Level:    logger.PENTACONTA,
			Time:     time.Now(),
		})
		shutdown(services)
	}

mainloop:
	for {
		select {
		case <-expired:
			exit()
			break mainloop
		case <-exitChannel:
			exit()
			break mainloop
		case <-evalTicker.C:
			eval.Loop()
		}
	}
	// wait a little so some log messages in the queue
	// have a chance to be written
	time.Sleep(time.Millisecond * 100)
}

func shutdown(services *runtime.Runtime) {
	for _, executor := range services.Executors {
		executor.Exit()
	}
	for _, executor := range services.Executors {
		for !executor.IsTerminated() {
			time.Sleep(time.Millisecond * 100)
		}
	}
}

func createAndStartFsTriggers(svs *runtime.Runtime, data *declaration.Root) {
	for name, fsTrigger := range data.FSTriggers {
		fsListener, err := runtime.NewFSTrigger(name, fsTrigger, svs)
		if err != nil {
			panic(err)
		}
		svs.FSListeners[name] = fsListener
		go func() {
			err := fsListener.Start()
			log.Fatal(err)
		}()
	}
}

func createAndStartServices(svs *runtime.Runtime, data *declaration.Root) {
	for name, service := range data.Services {
		executor, err := runtime.NewService(name, service, svs.Logs)
		if err != nil {
			panic(err)
		}

		svs.Executors[name] = executor
		go executor.Start()
	}
}

func readConfigParams() (string, bool) {
	var configName string
	var help bool
	executable := filepath.Base(os.Args[0])
	flags := flag.NewFlagSet("pentacota", flag.ContinueOnError)
	flags.StringVar(&configName, "config", executable, "name of config file to use, no .yaml or .json extension.")
	flags.BoolVar(&help, "help", false, "Print help text and exit")
	flags.Parse(os.Args[1:])
	return configName, help
}

func printHelp() {
	flag.PrintDefaults()
	fmt.Println("\nyaml declaration\n================")
	fmt.Println(declaration.Doc())
}

func readData(file string) (interface{}, error) {
	binData, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	data := interface{}(nil)

	if strings.HasSuffix(file, ".json") {
		err = json.Unmarshal(binData, &data)
		if err != nil {
			return nil, err
		}
		return data, nil
	} else if strings.HasSuffix(file, ".yaml") {
		err = yaml.Unmarshal(binData, &data)
		if err != nil {
			return nil, err
		}
		return data, nil
	}
	panic("Returned path must have .json or .yaml extension")
}

func probeLocation(path string) (string, error) {
	locations := []string{}
	if filepath.IsAbs(path) {
		locations = append(locations, path+".json")
		locations = append(locations, path+".yaml")
	} else {
		wd, err := os.Getwd()
		if err == nil {
			abspath := filepath.Join(wd, path)
			locations = append(locations, abspath+".json")
			locations = append(locations, abspath+".yaml")
		}
		abspath := filepath.Join("/etc", path)
		locations = append(locations, abspath+".json")
		locations = append(locations, abspath+".yaml")
	}

	for _, location := range locations {
		_, err := ioutil.ReadFile(location)
		if err == nil {
			return location, nil
		}
	}

	return "", fmt.Errorf("Could not find config file in locations %v", locations)
}
